import { Component } from "react";
import mainPicture from '../assets/images/people-playing-casino.jpg'
import dice from '../assets/images/dice.png'
import dice1 from '../assets/images/1.png'
import dice2 from '../assets/images/2.png'
import dice3 from '../assets/images/3.png'
import dice4 from '../assets/images/4.png'
import dice5 from '../assets/images/5.png'
import dice6 from '../assets/images/6.png'
class Content extends Component {
    constructor(props) {
        super(props);
        this.state = {
            random: 0
        }
    }
    onBtnDiceClick = () => {
        this.setState({
            random: Math.floor(Math.random() * 6) + 1
        })

    }
    imageDice(paramDice) {
        if (paramDice == 1) {
            return dice1;
        }
        else if (paramDice == 2) {
            return dice2;
        }
        else if (paramDice == 3) {
            return dice3;
        }
        else if (paramDice == 4) {
            return dice4;
        }
        else if (paramDice == 5) {
            return dice5;
        }
        else if (paramDice == 6) {
            return dice6;
        }
    }



    render() {

        return (
            <>
                <p>{this.state.random}</p>
                <img src={mainPicture} alt="cover"></img>

                <img src={this.state.random === 0 ? dice : this.imageDice(this.state.random)} alt={this.state.random} width={"100px"}></img>

                <button onClick={this.onBtnDiceClick}>Lac xi ngau</button>
            </>
        )
    }
}
export default Content